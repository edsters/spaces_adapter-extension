<?php

return [
    'access_key' => [
        'required' => TRUE,
        'type'     => 'anomaly.field_type.encrypted',
    ],
    'secret_key' => [
        'required' => TRUE,
        'type'     => 'anomaly.field_type.encrypted',
    ],
    'region'     => [
        'required' => TRUE,
        'type'     => 'anomaly.field_type.select',
        'config'   => [
            'options' => [
                'ams3' => 'Amsterdam 3',
                'nyc3' => 'New York 3',
                'sfo2' => 'San Francisco 2',
                'sgp1' => 'Singapore 1',
                'fra1' => 'Frankfurt 1'
            ],
        ],
    ],
    'bucket'     => [
        'required' => TRUE,
        'type'     => 'anomaly.field_type.text',
    ],
    'prefix'     => 'anomaly.field_type.text',
    'spaces_cdn' => 'anomaly.field_type.text'
];
