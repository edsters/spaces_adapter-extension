<?php

return [
    'access_key' => [
        'label'        => 'Access Key',
        'instructions' => 'Enter your spaces access key.',
    ],
    'secret_key' => [
        'label'        => 'Secret Key',
        'instructions' => 'Enter your spaces secret key.',
    ],
    'region'     => [
        'label'        => 'Region',
        'instructions' => 'Choose the region your spaces bucket resides in.',
    ],
    'bucket'     => [
        'label'        => 'Bucket',
        'instructions' => 'Enter your spaces bucket name.',
    ],
    'prefix'     => [
        'label'        => 'Prefix',
        'instructions' => 'Prefixes are useful for disks that share a single space.<br><strong>https://{bucket}.{region}.cdn.digitaloceanspaces.com/{prefix}/{folder}/{filename}</strong>',
    ],
    'spaces_cdn' => [
        'label'        => 'Spaces CDN Domain',
        'instructions' => '<a href="https://www.digitalocean.com/docs/spaces/how-to/enable-cdn/">Specify the Spaces CDN domain if available.</a>',
        'placeholder'  => 'images.mydomain.com',
    ],
];