<?php

return [
    'name'        => 'Spaces Adapter',
    'description' => 'A Digital Ocean Spaces adapter for the files module.',
];
