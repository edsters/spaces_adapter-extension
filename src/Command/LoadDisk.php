<?php namespace Emange\SpacesAdapterExtension\Command;

use Anomaly\ConfigurationModule\Configuration\Contract\ConfigurationRepositoryInterface;
use Anomaly\EncryptedFieldType\EncryptedFieldTypePresenter;
use Anomaly\FilesModule\Disk\Adapter\AdapterFilesystem;
use Anomaly\FilesModule\Disk\Contract\DiskInterface;
use Aws\S3\S3Client;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Filesystem\FilesystemManager;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\MountManager;

/**
 * Class LoadDisk
 * *
 *
 * @author        Edi Mange <edi@mange.biz>
 * @package       Emange\SpacesAdapterExtension\Command
 */
class LoadDisk
{

    /**
     * The disk interface.
     *
     * @var DiskInterface
     */
    protected $disk;

    /**
     * Create a new LoadDisk instance.
     *
     * @param DiskInterface $disk
     */
    public function __construct(DiskInterface $disk)
    {
        $this->disk = $disk;
    }


    public function handle(
        ConfigurationRepositoryInterface $configuration,
        FilesystemManager $filesystem,
        MountManager $manager,
        Repository $config
    )
    {
        /* @var EncryptedFieldTypePresenter $key */
        $key = $configuration->presenter('emange.extension.spaces_adapter::access_key', $this->disk->getSlug());

        /* @var EncryptedFieldTypePresenter $secret */
        $secret = $configuration->presenter('emange.extension.spaces_adapter::secret_key', $this->disk->getSlug());

        $bucket = $configuration->value('emange.extension.spaces_adapter::bucket', $this->disk->getSlug());

        $region = $configuration->value('emange.extension.spaces_adapter::region', $this->disk->getSlug());

        $prefix = $configuration->value('emange.extension.spaces_adapter::prefix', $this->disk->getSlug());

        $spaces_cdn = $configuration->value('emange.extension.spaces_adapter::spaces_cdn', $this->disk->getSlug());

        $config->set(
            'filesystems.disks.' . $this->disk->getSlug(),
            [
                'driver' => 's3',
                'key'    => $key->decrypt(),
                'secret' => $secret->decrypt(),
                'region' => $region,
                'bucket' => $bucket,
                'prefix' => $prefix,
            ]
        );

        $baseUrl = 'https://' . $bucket . '.' . $region . '.digitaloceanspaces.com';

        if ($spaces_cdn) {
            $baseUrl = trim($spaces_cdn, '/');
        }

        if ($prefix) {
            $baseUrl = $baseUrl . '/' . $prefix;
        }

        $driver = new AdapterFilesystem(
            $this->disk,
            new AwsS3Adapter(
                $client = new S3Client(
                    [
                        'credentials' => [
                            'key'    => $key->decrypt(),
                            'secret' => $secret->decrypt(),
                        ],
                        'region'      => $region,
                        'version'     => 'latest',
                        'endpoint'    => 'https://' . $region . '.digitaloceanspaces.com'
                    ]
                ),
                $bucket,
                $prefix
            ),
            [
                'base_url' => $baseUrl,
            ]
        );

        $manager->mountFilesystem($this->disk->getSlug(), $driver);

        $filesystem->extend(
            $this->disk->getSlug(),
            function () use ($driver) {
                return $driver;
            }
        );
    }
}
