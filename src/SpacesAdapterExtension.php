<?php namespace Emange\SpacesAdapterExtension;

use Anomaly\FilesModule\Disk\Adapter\AdapterExtension;
use Anomaly\FilesModule\Disk\Adapter\Contract\AdapterInterface;
use Anomaly\FilesModule\Disk\Contract\DiskInterface;
use Emange\SpacesAdapterExtension\Command\LoadDisk;

/**
 * Class SpacesAdapterExtension
 *
 * @author        Edi Mange <edi@mange.biz>
 * @package       Emange\SpacesAdapterExtension
 */
class SpacesAdapterExtension extends AdapterExtension implements AdapterInterface
{

    /**
     * This module provides the spaces
     * storage adapter for the files module.
     *
     * @var string
     */
    protected $provides = 'anomaly.module.files::adapter.spaces';

    /**
     * Load the disk.
     *
     * @param DiskInterface $disk
     */
    public function load(DiskInterface $disk)
    {
        $this->dispatch(new LoadDisk($disk));
    }

    /**
     * Validate adapter configuration.
     *
     * @param array $configuration
     * @return bool
     */
    public function validate(array $configuration)
    {
        return true;
    }
}
